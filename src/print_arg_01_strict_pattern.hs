import safe "base" Prelude ()
import safe "base" System.Environment
    ( getArgs )
import safe "base" System.IO
    ( IO
    , putStrLn
    )

main :: IO ()
main = do
    args <- getArgs
    case args of
        ~[] -> putStrLn "Empty."
        ~(fname : _) -> putStrLn fname
