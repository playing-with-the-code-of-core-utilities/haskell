import safe "base" Control.Exception
    ( IOException
    , try
    )
import safe "base" Data.Either
    ( Either -- ( Left, Right )
    , either
    )
-- import safe "base" Data.Eq
    -- ((==))
import safe "base" Data.Function
    ((.), ($)
    , id
    )
import safe "base" Data.List
    ( -- (++)
    null
    )
import safe "base" Data.Maybe
    ( listToMaybe
    -- , Maybe ( Just, Nothing )
    , maybe
    )
import safe "base" Data.String
    ( String )
import safe "base" System.Environment
    ( getArgs )
import safe "base" System.IO
    ( IO
    , putStrLn
    , readFile
    )
import safe "base" Text.Show
    ( show )

tryReadFile :: String -> IO (Either IOException String)
tryReadFile = try . readFile

main :: IO ()
main = do
    args <- getArgs
    if null args
        -- [] -> putStrLn ("This should not happens.")
        -- (a0 : [])
        -- "You called: " ++ a0 ++ 
        then putStrLn ("\nUsage: first arg is used, others are ignored")
        -- (_ : (fname : _))
        else do
            text <- tryReadFile $ maybe "" id $ listToMaybe args
            putStrLn $ either show id text
            -- case text of
                -- Left _ -> putStrLn $ show err
                -- Right _ -> putStrLn content
