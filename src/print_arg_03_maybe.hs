import safe "base" Data.Function
    ( ($)
    , id
    )
import safe "base" Data.Maybe
    ( listToMaybe
    , maybe
    )
import safe "base" System.Environment
    ( getArgs )
import safe "base" System.IO
    ( IO
    , putStrLn
    )

main :: IO ()
main = do
    args <- getArgs
    putStrLn
        $ maybe "" id
        $ listToMaybe args
