import safe "base" Control.Exception
    ( IOException
    , try
    )
import safe "base" ata.Either
    ( Either -- ( Left, Right )
    , either
    )
-- import safe "base" Data.Eq
    -- ((==))
import safe "base" Data.Function
    ((.), ($)
    , id
    )
import safe "base" Data.List
    ( -- (++)
    null
    )
import safe "base" Data.Maybe
    ( listToMaybe
    -- , Maybe ( Just, Nothing )
    , maybe
    )
import safe "base" Data.String
    ( String )
import safe "base" System.Environment
    ( getArgs )
import safe "base" System.IO
    ( IO
    , putStrLn
    , readFile
    )
import safe "base" Text.Show
    ( show )

main :: IO ()
main = do
    args <- getArgs
    if null args
        then putStrLn ("Empty.")
        else putStrLn $ maybe "" id $ listToMaybe args
